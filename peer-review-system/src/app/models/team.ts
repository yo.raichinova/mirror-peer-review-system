export interface Team {
    uid?: string;
    teamName?: string;
    members?: [];
}
