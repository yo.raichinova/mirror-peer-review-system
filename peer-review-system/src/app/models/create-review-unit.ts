export interface CreateReviewUnit {
    uid: string;
    title: string;
    content: string;
    tags: [];
    ruOwner: string;
    reviewers: [];
    comments: [];
    status: string;
    date: Date;
    team: string;
}
