import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { TeamsComponent } from './components/teams/teams.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full', data: { animation: 'isRight, isLeft' } },
  { path: 'home', component: HomeComponent, data: { animation: 'isRight, isLeft' } },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule', data: { animation: 'isLeft, isRight' } },
  { path: 'review-units', loadChildren: './components/review-unit/review-unit.module#ReviewUnitModule', data: { animation: 'isRight' }},
  { path: 'users', loadChildren: './components/users/users.module#UsersModule', data: { animation: 'isRight, isLeft' }},
  { path: 'teams', loadChildren: './components/teams/teams.module#TeamsModule', data: { animation: 'isRight' } },
  { path: 'not-found', component: NotFoundComponent, data: { animation: 'isRight' } },
  { path: 'server-error', component: ServerErrorComponent, data: { animation: 'isRight' } },

  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }), BrowserAnimationsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
