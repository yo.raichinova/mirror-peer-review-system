import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { AgGridModule } from 'ag-grid-angular';
import { TimestampComponent } from './timestamp/timestamp.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { TagsComponent } from './tags/tags.component';
import { UsersCellCustomComponent } from '../components/users/users-cell-custom/users-cell-custom.component';
import { ReviewCellCustomComponent } from '../components/review-unit/review-cell-custom/review-cell-custom.component';
import { TeamsCellCustomComponent } from '../components/teams/teams-cell-custom/teams-cell-custom.component';

@NgModule({
  declarations: [TimestampComponent, TagsComponent, UsersCellCustomComponent, ReviewCellCustomComponent, TeamsCellCustomComponent],
  imports: [
    AngularMultiSelectModule,
    AgGridModule,
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgBootstrapFormValidationModule,
    NgSelectModule
  ],
  exports: [
    AngularMultiSelectModule,
    AgGridModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    NgBootstrapFormValidationModule,
    NgSelectModule
  ],
  entryComponents: [TimestampComponent, TagsComponent, UsersCellCustomComponent, ReviewCellCustomComponent, TeamsCellCustomComponent],
  providers: [],
})
export class SharedModule { }
