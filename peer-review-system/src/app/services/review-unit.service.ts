import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { ReviewUnit } from '../models/review-unit';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ReviewUnitService {
  public form: FormGroup;
  public commentForm: FormGroup;
  public ruCollection: AngularFirestoreCollection<ReviewUnit>;
  public reviewUnits: Observable<ReviewUnit[]>;
  public ruDoc: AngularFirestoreDocument<ReviewUnit>;
  public userId: string;
  newRevUnit;

  constructor(
    public formBuilder: FormBuilder,
    private toastr: ToastrService,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
      }
    });

    this.commentForm = new FormGroup({
      content: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
    });

    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      content: ['', [Validators.required]],
      tags: [''],
      reviewers: [['']],
      team: [''],
      status: [{ value: 'Pending', disabled: true }]
    });


    this.ruCollection = this.afs.collection<ReviewUnit>('review-units');
  }

  public async createReviewUnit(revUnit, ownerId: string, author: string, ownerEmail: string, rUDate, selectedReviewers, selectedTeam) {
    this.newRevUnit = await this.ruCollection
      .add({
        title: revUnit.title,
        content: revUnit.content,
        date: rUDate,
        tags: revUnit.tags,
        ruOwner: { displayName: author, uid: ownerId, email: ownerEmail },
        status: 'Pending',
        reviewers: selectedReviewers,
        team: selectedTeam
      });
    this.newRevUnit.update({ uid: this.newRevUnit.id }).catch(error => this.toastr.error(error.message));
    this.toastr.success(`You have successfully submitted a review unit!`);
    this.router.navigate(['users/dashboard']);
    return this.newRevUnit;
  }

  public updateReviewUnitStatusField(reviewUnit, statusUpdate) {
    this.ruCollection.doc(reviewUnit).update({ status: statusUpdate }).then(() => {
      this.toastr.success('You have successfully submitted a review for this unit!');
      this.router.navigate(['users/dashboard']);
    }).catch(error => this.toastr.error(error.message));
  }

  public updateReviewUnitFeedback(revUnit: string, commentContent, reviewerName: string) {
    this.ruCollection.doc(revUnit).collection('comments').add({
      content: commentContent,
      author: reviewerName
    });
  }

  public adminUpdateReviewUnitStatusField(reviewUnit, statusUpdate) {
    this.ruCollection.doc(reviewUnit).update({ status: statusUpdate }).then(() => {
      this.toastr.success(`Review unit's status has been updated!`);
    }).catch(error => this.toastr.error(error.message));
  }

  public updateStatusByReviewer(revUnit: string, reviewerName: string, reviewerId: string, status: string) {
    this.ruCollection.doc(revUnit).collection('status-updates').add({
      reviewerAuthor: reviewerName,
      uid: reviewerId,
      status,
    });
  }

  deleteReviewUnit(revUnit) {
    this.ruDoc = this.afs.doc(`review-units/${revUnit.uid}`);
    this.ruDoc.delete();
  }

  public getReviewUnitById(revUnitKey) {
    return this.ruCollection.doc(revUnitKey).valueChanges();
  }
  public getAllReviewUnits() {
    return this.ruCollection.valueChanges();
  }
  public getRevUnitComments(revUnitKey) {
    return this.ruCollection.doc(revUnitKey).collection('comments').valueChanges();
  }

  public getRevUnitFilesList(revUnitKey) {
    return this.ruCollection.doc(revUnitKey).collection('files').valueChanges();
  }
}
