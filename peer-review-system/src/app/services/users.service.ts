import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { ToastrService } from 'ngx-toastr';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public currentUser$: Observable<User | null>;
  public user;
  public users;
  public userId;
  public teamId;
  public teamsColRef;
  public teamNames;
  public userHasTeam;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private toastr: ToastrService
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
      }
    });
  }

  async getAllUsers() {
    this.afs.collection('users').valueChanges().subscribe(val => {
      return this.users = val;
    });
  }

  public getUserTeams(userId) {
    return this.afs.collection('teams', ref => ref.where('members', 'array-contains', userId)).valueChanges();
  }

  public sendUserPasswordResetEmail(email: string): Promise<void> {
    return this.afAuth.auth.sendPasswordResetEmail(email)
      .then(() => {
        this.toastr.success('A reset password email was sent to you.');
      })
      .catch((error) => {
        this.toastr.error(error.message);
      });
  }

  public getUserRequests(userId) {
    return this.afs.collection('review-units', ref => ref.where('ruOwner.uid', '==', userId)).valueChanges();
  }

  public getTeamRequests(teamId) {
    return this.afs.collection('review-units', ref => ref.where('team', '==', teamId)).valueChanges();
  }

  public hasATeam(userId) {
    return this.userHasTeam = this.afs.collection('users', ref =>
      ref.where('uid', '==', userId)
        .where('teams', '==', [])).valueChanges();
  }

  public makeAdmin(userId) {
    const userQuery = this.afs.collection('users', ref =>
      ref.where('uid', '==', userId));
    return userQuery.doc(userId).update({
      role: 'admin'
    })
      .then(() => this.toastr.success('This member has been promoted to administrator!'))
      .catch(err => this.toastr.error(err.message)
      );
  }

  deleteUser(userId, teamId) {
    this.afs.doc(`users/${userId}`).delete()
    .catch(err => this.toastr.error(err.message));
    const teamQuery = this.afs.collection('teams', ref =>
      ref.where('members', 'array-contains', userId));
    console.log(teamQuery);
    teamQuery.doc(teamId).update({
    members: firebase.firestore.FieldValue.arrayRemove(userId)
    })
    .then(() => this.toastr.success('This member has been successfully deleted from the database!'))
    .catch(err => this.toastr.error(err.message));
  }

}
