import { TestBed, inject } from '@angular/core/testing';
import { UsersService } from './users.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { of } from 'rxjs';

describe('UsersService', () => {

  // const afAuthSpy = jasmine.createSpyObj('AngularFireAuth', ['authState', 'sendPasswordResetEmail', 'delete']);
  const afsSpy = jasmine.createSpyObj('AngularFirestore', ['collection']);
  const userIdTest = 'sdqwqf0103f13f';
  const teamIdTest = 'wefwegfowef82f3f';
  const emailTest = 'test@test.com';
  const usersTest = [];
  const returnResult = {
    userId: 'wdwefhwoef7wef2dfewf',
  };
  const collectionSpy = jasmine.createSpyObj({ valueChanges: of(returnResult) });
  const afSpy = jasmine.createSpyObj('AngularFirestore', {
    collection: collectionSpy
  });
  const authStub: any = {
    authState: {},
    auth: {
      sendPasswordResetEmail() {
        return Promise.resolve();
      },
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UsersService,
        {
          provide: AngularFireAuth,
          useValue: authStub,
        },
        {
          provide: AngularFirestore,
          useValue: afSpy
        }
      ]
    });
    // service = TestBed.get(UsersService);
    authStub.authState = of(null);
  });

  it('should be created', inject([UsersService], (service: UsersService) => {
    expect(service).toBeTruthy();
  }));

  it('should call sendUserPasswordResetEmail', inject([UsersService], (service: UsersService) => {
    const mock = TestBed.get(AngularFireAuth);
    const spy = spyOn(authStub.auth, 'signInWithEmailAndPassword').and.callThrough();
    mock.auth = authStub.auth;
    service.sendUserPasswordResetEmail(emailTest);
    expect(spy).toHaveBeenCalledWith(emailTest);
  }));

  /*  it('should have a getAllUsers method', () => {
     service.getAllUsers();
     expect(collectionSpy.valueChanges).toHaveBeenCalled();
     expect(afSpy.collection).toHaveBeenCalledWith('fakeCollection');
   }); */

  it('should call a getUserTeams() method', inject([UsersService], (service: UsersService) => {
    const mock = TestBed.get(AngularFireAuth);
    const spy = spyOn(authStub.auth, 'signInWithEmailAndPassword').and.callThrough();
    mock.auth = authStub.auth;
    service.getUserTeams(userIdTest);
    expect(spy).toHaveBeenCalledWith(userIdTest);
  }));

  it('should have a getUserRequests() method', () => {
    const service: UsersService = TestBed.get(UsersService);
    service.getUserRequests(userIdTest);
    expect(collectionSpy.valueChanges).toHaveBeenCalled();
    expect(afSpy.collection).toHaveBeenCalledWith('fakeCollection');
  });

  it('should have a getTeamRequests() method', () => {
    const service: UsersService = TestBed.get(UsersService);
    service.getTeamRequests(teamIdTest);
    expect(collectionSpy.valueChanges).toHaveBeenCalled();
    expect(afSpy.collection).toHaveBeenCalledWith('fakeCollection');
  });




});
