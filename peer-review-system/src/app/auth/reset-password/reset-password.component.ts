import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-reset-password',
  animations: [slider],
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})

export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public auth: AuthService,
    public usersService: UsersService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
    });
  }

  get email() {
    return this.resetPasswordForm.get('email');
  }

  resetPassword(): void {
    this.usersService.sendUserPasswordResetEmail(this.email.value)
    .then(user => {
      this.toastr.success('Reset password is sent to your email');
      this.router.navigate(['/']);
    });
  }

}
