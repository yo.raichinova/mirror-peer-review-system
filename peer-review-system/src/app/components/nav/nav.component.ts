import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { ReviewUnitService } from 'src/app/services/review-unit.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  public isNavbarCollapsed = true;

  public constructor(
    public auth: AuthService,
    public users: UsersService,
    public reviewUnits: ReviewUnitService,
  ) { }

}
