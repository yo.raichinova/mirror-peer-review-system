import { Component, OnInit } from '@angular/core';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-home',
  animations: [slider],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
