import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'firebase';
import { Subscription, Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { GridOptions } from 'ag-grid-community';
import { UsersCellCustomComponent } from './users-cell-custom/users-cell-custom.component';
import { RouterOutlet } from '@angular/router';
import { fader, slider } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-users',
  animations: [slider],
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {
  private gridApi;
  private gridColumnApi;

  columnDefs = [
    {
      headerName: 'Avatar', field: 'photoURL', sortable: false, filter: false,
      cellRenderer: (params) => `<img src="${params.value}"
    class="img-responsive img-thumbnail rounded-circle avatar" width="75" height="75">`},
    { headerName: 'DisplayName', field: 'displayName', sortable: true, filter: true },
    { headerName: 'Role', field: 'role', sortable: true, filter: true },
    { headerName: 'E-mail', field: 'email', sortable: true, filter: true },
    {headerName: 'Actions', field: 'action', cellRendererFramework: UsersCellCustomComponent}
    // { headerName: 'Teams', field: 'teams', sortable: true, filter: true, cellRenderer: (params) => params.value },
  ];

  rowData: Observable<any>;

  constructor(
    public usersService: UsersService,
    private afs: AngularFirestore
  ) {
  }
  public users;
  ngOnInit() {
    this.rowData = this.afs.collection('users').valueChanges();

    /* this.afs.collection('users').valueChanges().subscribe(val => {
      return this.users = val;
    }); */
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

}
