import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder } from '@angular/forms';
import { TeamsService } from 'src/app/services/teams.service';
import { Observable } from 'rxjs';
import { TimestampComponent } from 'src/app/shared/timestamp/timestamp.component';
import { Router } from '@angular/router';
import { TagsComponent } from 'src/app/shared/tags/tags.component';
import { GridOptions, ValueGetterParams } from 'ag-grid-community';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-dashboard',
  animations: [slider],
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public displayName: string;
  public userId: string;
  public teams;
  public gridApi;
  public gridColumnApi;
  public rowData: Observable<any>;
  public teamValue;
  public selectedTeam;
  public members;
  public tag;
  public userHasATeam: boolean;
  public gridOptions: GridOptions = {} as GridOptions;
  public tags;

  columnDefs = [
    {
      headerName: 'Title', field: 'title', sortable: true, filter: true,
      cellRenderer: (params) => `<a href="/review-units/${params.data.uid}">${params.data.title}</a>`,
      cellStyle: { 'white-space': 'normal' }
    },
    { headerName: 'Status', field: 'status', sortable: true, filter: true },
    {
      headerName: 'Owner', field: 'ruOwner.displayName', sortable: true, filter: true
    },
    {
      headerName: 'Date', field: 'date', cellRendererFramework: TimestampComponent,
      sortable: true, filter: 'agDateColumnFilter'
    },
    { headerName: 'Tags', field: 'tags', sortable: true, filter: 'agTextColumnFilter',
    filterValueGetter: (params: ValueGetterParams) => {
      const tags = params.data.tags.map(tag => tag.itemName).join(', ');
      return tags;
    },
    cellRenderer: (params) => {
      const tags = params.data.tags.map(tag => `<p class="mx-1 badge badge-info">${tag.itemName}</p>`).join('');
      return tags;
    },
    cellStyle: { 'white-space': 'normal' }
  },
  ];

  constructor(
    public auth: AuthService,
    public usersService: UsersService,
    private afAuth: AngularFireAuth,
    private router: Router,
    public teamsService: TeamsService) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.displayName = user.displayName;
        this.userId = user.uid;
        this.doesUserHaveATeam();
        this.rowData = this.usersService.getUserRequests(this.userId);
        this.usersService.getUserTeams(this.userId).subscribe(
          teams => this.teams = teams);
      }
    });

    this.tags = {
      1: 'article',
      2: 'code review' ,
      3: 'diagram' ,
      4: 'document',
      5: 'forecast',
      6: 'scheme',
      7: 'important',
      8: 'projection',
      9: 'urgent',
      10: 'editorial'
      };
  }

  ngOnInit() {
  }

  public getUserRequests() {
    return this.rowData = this.usersService.getUserRequests(this.userId);
  }

  public setTeamValue() {
    this.teamValue = this.selectedTeam;
    this.members = this.teamsService.getTeamMembers(this.teamValue);
    this.rowData = this.usersService.getTeamRequests(this.teamValue);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  public doesUserHaveATeam() {
    this.usersService.hasATeam(this.userId).subscribe((response) => {
      if (response.length === 0) {
        return this.userHasATeam = true;
      }
      return this.userHasATeam = false;
    });
  }

}
