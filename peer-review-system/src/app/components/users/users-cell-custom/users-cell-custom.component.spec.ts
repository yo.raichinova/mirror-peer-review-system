import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersCellCustomComponent } from './users-cell-custom.component';

describe('UsersCellCustomComponent', () => {
  let component: UsersCellCustomComponent;
  let fixture: ComponentFixture<UsersCellCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersCellCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersCellCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
