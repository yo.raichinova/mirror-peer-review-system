import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { Router, RouterOutlet } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TeamsService } from 'src/app/services/teams.service';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { fader, slider } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-user-profile',
  animations: [slider],
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  avatarForm: FormGroup;
  avatarURL: string;
  displayNameForm: FormGroup;
  displayName: string;
  userId: string;
  teams;
  selectedTeam;
  members;
  teamValue;
  userHasATeam;
  public userRequests: number;


  constructor(
    public auth: AuthService,
    public usersService: UsersService,
    private toastr: ToastrService,
    private afAuth: AngularFireAuth,
    private router: Router,
    private formBuilder: FormBuilder,
    public teamsService: TeamsService
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.displayName = user.displayName;
        this.userId = user.uid;
        this.doesUserHaveATeam();
        this.usersService.getUserTeams(this.userId).subscribe(
          teams => this.teams = teams);
        this.usersService.getUserRequests(this.userId).subscribe((response) => {
          this.userRequests = response.length;
        });
      }
    });
  }

  ngOnInit() {
    this.avatarForm = this.formBuilder.group({
      avatarURL: ['',
        [
          Validators.required,
          Validators.minLength(5)]
      ],
    });

    this.displayNameForm = this.formBuilder.group({
      displayName: ['',
        [
          Validators.required,
          Validators.minLength(5)]
      ],
    });
  }

  get userDataMethod() {
    return this.auth.userData;
  }

  public setTeamValue() {
    this.teamValue = this.selectedTeam;
    this.members = this.teamsService.getTeamMembers(this.teamValue);
  }

  public leaveTeam() {
    this.teamsService.userLeavesTeam(this.userId, this.teamValue);
    this.teamsService.removeUserFromTeam(this.userId, this.teamValue);
  }

  get avatarURLfield() {
    return this.avatarForm.get('avatarURL');
  }

  public changeAvatar() {
    const user = this.afAuth.auth.currentUser;
    user.updateProfile({
      photoURL: this.avatarURLfield.value
    }).then(() => {
      this.toastr.success(`Your avatar was updated successfully.`);
      this.auth.updateUserData(user);
    }).catch((error) => {
      this.toastr.error(error.message);
    });
  }

  get displayNamefield() {
    return this.displayNameForm.get('displayName');
  }

  public changeDisplayName() {
    const user = this.afAuth.auth.currentUser;
    user.updateProfile({
      displayName: this.displayNamefield.value
    }).then(() => {
      this.toastr.success(`Your display name was updated successfully!`);
      this.auth.updateUserData(user);
    }).catch((error) => {
      this.toastr.error(error.message);
    });
  }

  public doesUserHaveATeam() {
    this.usersService.hasATeam(this.userId).subscribe((response) => {
      if (response.length === 0) {
        return this.userHasATeam = true;
      }
      return this.userHasATeam = false;
    });
  }

  public passwordReset(): void {
    this.usersService.sendUserPasswordResetEmail(this.afAuth.auth.currentUser.email);
  }
}
