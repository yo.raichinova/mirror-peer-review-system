import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersService } from 'src/app/services/users.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { TeamsService } from 'src/app/services/teams.service';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { TeamsCellCustomComponent } from './teams-cell-custom/teams-cell-custom.component';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-teams',
  animations: [slider],
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {

  public displayName: string;
  public userId: string;
  public teams;
  public teamValue;
  public selectedTeam;
  public rowData: Observable<any>;
  public rowSelection;
  private gridApi;
  private gridColumnApi;
  public selectedMembers;
  public selectedIds: string[];
  public updateForm;


  columnDefs = [
    {
      headerName: 'Avatar', field: 'photoURL', sortable: false, filter: false,
      cellRenderer: (params) => `<img src="${params.value}"
    class="img-responsive img-thumbnail rounded-circle avatar" width="75" height="75">`},
    {
      headerName: 'DisplayName', field: 'displayName', sortable: true, filter: true,
      cellRenderer: 'agGroupCellRenderer'
    },
    { field: 'uid', hide: true },
    { headerName: 'Actions', field: 'action', cellRendererFramework: TeamsCellCustomComponent }
  ];

  constructor(
    public auth: AuthService,
    public usersService: UsersService,
    public teamsService: TeamsService,
    public afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.displayName = user.displayName;
        this.userId = user.uid;
        this.teams = this.teamsService.getAllTeams();
      }
    });
  }

  ngOnInit() {

    this.updateForm = this.teamsService.form;
  }



  public setTeamValue() {
    this.teamValue = this.selectedTeam;
    this.teamsService.selectedTeam = this.teamValue;
    this.rowData = this.teamsService.getTeamMembers(this.teamValue);
  }

  public onSelectionChanged() {
    const selectedRows = this.gridApi.getSelectedRows();
    let selectedRowsString = '';
    selectedRows.forEach((selectedRow, index) => {
      if (index !== 0) {
        selectedRowsString += ', ';
      }
      selectedRowsString += selectedRow.displayName;
      this.selectedMembers = selectedRows;
      this.selectedIds = this.selectedMembers.map(el => el.uid);
      return this.selectedIds;
    });
    if (selectedRows.length >= 5) {
      selectedRowsString += ' - and ' + (selectedRows.length - 5) + ' others';
    }
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
  }

  public onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

 
}
