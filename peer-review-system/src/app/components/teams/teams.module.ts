import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsComponent } from './teams.component';
import { Routes, RouterModule } from '@angular/router';
import { CreateTeamComponent } from './create-team/create-team.component';
import { AuthGuardComponent } from 'src/app/shared/guards/auth-guard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UpdateTeamComponent } from './update-team/update-team.component';

const routes: Routes = [
  { path: 'all', component: TeamsComponent },
  { path: 'new-team', component: CreateTeamComponent, data: { title: 'Create a new team' }, canActivate: [AuthGuardComponent] },
  { path: 'update-team', component: UpdateTeamComponent, data: { title: 'Add new members to team' , canActivate: [AuthGuardComponent]} }
  ];

@NgModule({
  declarations: [TeamsComponent, CreateTeamComponent, UpdateTeamComponent],
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class TeamsModule { }
