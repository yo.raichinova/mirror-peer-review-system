import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TeamsService } from 'src/app/services/teams.service';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-create-team',
  animations: [slider],
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss']
})
export class CreateTeamComponent implements OnInit {
  public rowSelection;
  private gridApi;
  private gridColumnApi;
  private gridOptions;
  public selectedMembers = [];
  public selectedIds = [];
  public teamForm;
  public teamName;
  public userId: string;
  rowData: Observable<any>;

  columnDefs = [
    {
      headerName: 'Avatar', field: 'photoURL', sortable: false, filter: false,
      cellRenderer: (params) => `<img src="${params.value}"
    class="img-responsive img-thumbnail rounded-circle avatar" width="75" height="75">`},
    {
      headerName: 'DisplayName', field: 'displayName', sortable: true, filter: true,
      cellRenderer: 'agGroupCellRenderer'
    },
    { field: 'uid', hide: true }
  ];

  constructor(
    public usersService: UsersService,
    private afs: AngularFirestore,
    private formBuilder: FormBuilder,
    private afAuth: AngularFireAuth,
    public teamsService: TeamsService
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
      }
    });
   }

  ngOnInit() {
    this.rowSelection = 'multiple';
    this.rowData = this.afs.collection('users').valueChanges();

    this.teamForm = this.teamsService.form;
  }

  onSelectionChanged(event) {
    const selectedRows = this.gridApi.getSelectedRows();
    let selectedRowsString = '';
    selectedRows.forEach((selectedRow, index) => {
      // if (index > 5) {
      //   return;
      // }
      if (index !== 0) {
        selectedRowsString += ', ';
      }
      selectedRowsString += selectedRow.displayName;
      this.selectedMembers = selectedRows;
      this.selectedIds = this.selectedMembers.map(el => el.uid);
      this.selectedIds.push(this.userId);
      return this.selectedIds;
    });
    if (selectedRows.length >= 5) {
      selectedRowsString += ' - and ' + (selectedRows.length - 5) + ' others';
    }
    document.querySelector('#selectedRows').innerHTML = selectedRowsString;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
  }

  resetFields() {
    this.teamForm = this.teamsService.form;
  }

  onSubmit() {
    const teamName = this.teamsService.form.value;
    const members = this.selectedIds;
    this.teamsService.createTeam(teamName, members);
  }
}
