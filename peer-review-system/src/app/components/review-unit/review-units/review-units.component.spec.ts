import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewUnitsComponent } from './review-units.component';

describe('ReviewUnitsComponent', () => {
  let component: ReviewUnitsComponent;
  let fixture: ComponentFixture<ReviewUnitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewUnitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewUnitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
