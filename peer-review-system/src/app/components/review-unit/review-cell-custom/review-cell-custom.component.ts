import { Component, OnInit } from '@angular/core';
import { ReviewUnitService } from 'src/app/services/review-unit.service';

@Component({
  selector: 'app-review-cell-custom',
  templateUrl: './review-cell-custom.component.html',
  styleUrls: ['./review-cell-custom.component.scss']
})
export class ReviewCellCustomComponent {
  data: any;
  params: any;

  constructor(public revUnitService: ReviewUnitService) { }

  agInit(params) {
    this.params = params;
    this.data = params.value;
  }

  rejectReview() {
    const rowData = this.params;
    const statusUpdate = 'rejected';
    this.revUnitService.adminUpdateReviewUnitStatusField(rowData.data.uid, statusUpdate);
  }

  acceptReview() {
    const rowData = this.params;
    const statusUpdate = 'accepted';
    this.revUnitService.adminUpdateReviewUnitStatusField(rowData.data.uid, statusUpdate);
  }
}
