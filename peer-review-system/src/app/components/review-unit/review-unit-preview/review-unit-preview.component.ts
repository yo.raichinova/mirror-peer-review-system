import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from 'src/app/services/auth.service';
import { ReviewUnitService } from 'src/app/services/review-unit.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { ReviewUnitResolver } from './review-unit-preview-resolver';
import { switchMap } from 'rxjs/operators';
import { slider, fader } from 'src/app/shared/animations/route-animations';

@Component({
  selector: 'app-review-unit-preview',
  animations: [slider],
  templateUrl: './review-unit-preview.component.html',
  styleUrls: ['./review-unit-preview.component.scss']
})
export class ReviewUnitPreviewComponent implements OnInit {
  public reviewUnit;
  public reviewUnitComment;
  public selectedStatusItem = 'Pending';
  public statusItems: string[] = [];
  public commentForm;
  public reviewerDisplayName;
  public reviewerId;
  public tags: string[] = [];
  public comments;
  public files;
  public isRevUnitFeedbackDisabled = false;


  constructor(
    public auth: AuthService,
    public revUnitService: ReviewUnitService,
    public afs: AngularFirestore,
    public revUnitResolver: ReviewUnitResolver,
    private afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.reviewerDisplayName = user.displayName;
        this.reviewerId = user.uid;
      }
    });
  }

  ngOnInit() {
    this.route.params.pipe(switchMap(params => {

      const revUnitUrl = params.uid;
      this.displayComments(revUnitUrl);
      this.displayUploadedFiles(revUnitUrl);
      return this.revUnitService.getReviewUnitById(revUnitUrl);
    }))
      .subscribe(reviewUnit => {
        this.reviewUnit = reviewUnit;
        this.reviewUnit.tags.forEach(el => this.tags.push(el.itemName));
        if (this.reviewUnit.status === 'Rejected') {
          this.isRevUnitFeedbackDisabled = true;
        }
      });

    this.statusItems = ['Pending', 'Under review', 'Change requested', 'Accepted', 'Rejected'];

    this.commentForm = this.revUnitService.commentForm;
  }

  resetFields() {
    this.commentForm = this.revUnitService.commentForm;
  }

  onSubmit() {
    this.reviewUnitComment = this.commentForm.value;
    this.revUnitService.updateReviewUnitFeedback(this.reviewUnit.uid, this.reviewUnitComment.content, this.reviewerDisplayName);
    this.revUnitService.updateStatusByReviewer(this.reviewUnit.uid, this.reviewerDisplayName, this.reviewerId, this.selectedStatusItem);
    this.revUnitService.updateReviewUnitStatusField(this.reviewUnit.uid, this.selectedStatusItem);

    this.resetFields();
  }

  public displayUploadedFiles(revUnitId) {
    this.files = this.revUnitService.getRevUnitFilesList(revUnitId);
    console.log(this.files);
  }

  public displayComments(revUnitId) {
    this.comments = this.revUnitService.getRevUnitComments(revUnitId);
  }
}
