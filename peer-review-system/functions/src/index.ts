import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as sgMail from '@sendgrid/mail';

admin.initializeApp();
// const db = admin.firestore();
const API_KEY = functions.config().sendgrid.key;
const TEMPLATE_ID = functions.config().sendgrid.template;
sgMail.setApiKey(API_KEY);

exports.firestoreEmail = functions.firestore
    .document('review-units/{ruId}')
    .onCreate((snap, context) => {
        const ruId= context.params.ruId;  // <- Here is the change

        const db = admin.firestore()

        return db.collection('review-units').doc(ruId)
            .get()
            .then(doc => {
                const reviewUnit = doc.data()

                const msg = {
                    to: reviewUnit.ruOwner.email,
                    from: 'peer.review.system42@gmail.com',
                    templateId: TEMPLATE_ID,
                    dynamic_template_data: {
                        subject: 'You have successfully added a new unit for review!',
                        text: 'You have added a review unit in the review requests queue. Expect feedback quite soon. Thank you!',
                        name: reviewUnit.ruOwner.displayName,
                },
            };
                return sgMail.send(msg)
            })
            .then(() => console.log('email sent!'))
            .catch(err => console.log(err))
    });


    /* export const welcomeEmail = functions.auth.user().onCreate(user => {

    const msg = {
        to: user.email,
        from: 'peer.review.system42@gmail.com',
        templateId: TEMPLATE_ID,
        dynamic_template_data: {
            subject: 'Welcome to Mirror Peer Review System!',
            name: user.displayName,
        },
    };

    return sgMail.send(msg);

}); */
/* exports.newReviewRequestNotification = functions.firestore
    .document('review-units/{reviewUnitId}')
    .onCreate(async event => {
        const data: any = event.data()
        const userId = data.userId
        const reviewUnit = data.reviewUnitId
        // Notification content
        const payload = {
            notification: {
                title: 'New Review Request',
                body: `${reviewUnit} has been added and you have been kindly asked to provide your feedback!`,
                icon: './../../src/assets/icons/favicon-32x32.png'
            }
        }

        // ref to the parent document
        const db = admin.firestore();
        const usersRef = db.collection('FCMTokens').where('uid', '==', userId);

        // get users tokens and send notifications

        const users = await usersRef.get();

        const tokens: any = [];

        users.forEach(result => {
            const token = result.data().token;
            tokens.push(token);
        })

        return admin.messaging().sendToDevice(tokens, payload).then(function (response) {
            console.log("Successfully sent message: ", JSON.stringify(response));
            return;
        }).catch(function (error) {
            console.log("Error sending message: ", error);
            return;
        });

    }); */

/* exports.sendToDeviceG = functions.firestore
    .document('review-units/{reviewUnitID}')
    .onCreate(async snapshot => {
        const db = admin.firestore();
        const fcm = admin.messaging();


        const reviewUnit: any = snapshot.data();
        const ruOwnerId: any = reviewUnit.ruOwner.uid;

        const tokens: string[] = await db
            .collection('fcmTokens')
            .doc(ruOwnerId)
            .get()
            .then(tokensArray => {
                const docRef: string[] = tokensArray.data().tokens
                return docRef;
            });

        // const tokens = querySnapshot.map(snap => snap.id);

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: 'New Review Unit added successfully!',
                body: `You succeeded adding a review unit ${reviewUnit.title} containing ${reviewUnit.content}`,
                icon: 'your-icon-url',
                // click_action: 'NOTIFICATION_CLICK'
            }
        };

        return fcm.sendToDevice(tokens, payload).then(function (response) {
            console.log("Successfully sent message: ", JSON.stringify(response));
            return;
        }).catch(function (error) {
            console.log("Error sending message: ", error);
            return;
        });
    });
 */

// Sends email via HTTP. Can be called from frontend code. 
/* export const genericEmail = functions.https.onCall(async (data, context) => {

    if (!context.auth && !context.auth.token.email) {
        throw new functions.https.HttpsError('failed-precondition', 'Must be logged with an email address');
    }

    const msg = {
        to: context.auth.token.email,
        from: 'hello@fireship.io',
        templateId: TEMPLATE_ID,
        dynamic_template_data: {
            subject: data.subject,
            name: data.text,
        },
    };

    await sgMail.send(msg);

    // Handle errors here

    // Response must be JSON serializable
    return { success: true };

});


/*export const newComment = functions.firestore.document('posts/{postId}/comments/{commentId}').onCreate( async (change, context) => {

    // Read the post document
    const postSnap = await db.collection('posts').doc(context.params.postId).get();

    // Raw Data
    const post = postSnap.data();
    const comment = change.data();

    // Email
    const msg = {
        to: post.authorEmail,
        from: 'hello@fireship.io',
        templateId: TEMPLATE_ID,
        dynamic_template_data: {
            subject: `New Comment on ${post.title}`,
            name: post.displayName,
            text: `${comment.user} said... ${comment.text}`
        },
    };

    // Send it
    return sgMail.send(msg);

}); */

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });


/* exports.fcmSend = functions.database.ref('/messages/{userId}/{messageId}').onCreate(event => {


    const message = event.after.val()
    const userId  = event.params.userId

    const payload = {
          notification: {
            title: message.title,
            body: message.body,
            icon: "https://placeimg.com/250/250/people"
          }
        };


     admin.database()
          .ref(`/fcmTokens/${userId}`)
          .once('value')
          .then(token => token.val() )
          .then(userFcmToken => {
            return admin.messaging().sendToDevice(userFcmToken, payload)
          })
          .then(res => {
            console.log("Sent Successfully", res);
          })
          .catch(err => {
            console.log(err);
          });

  });
 */